package com.trandung.autoimess.sms.config.error;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;


@RestControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(BadRequestException.class)
    public @ResponseBody ResponseEntity<Object> handleBadRequestException(Exception ex) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Map<String, String> result = new HashMap<>();
        result.put("code", "ERROR");
        result.put("message", ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(headers).body(result);
    }
}
