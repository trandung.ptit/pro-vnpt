package com.trandung.autoimess.sms.api;

import com.trandung.autoimess.sms.api.request.CreateListSmsRequest;
import com.trandung.autoimess.sms.api.request.MultipleContentDTO;
import com.trandung.autoimess.sms.api.response.ResponseBodyDTO;
import com.trandung.autoimess.sms.model.MultipleContent;
import com.trandung.autoimess.sms.service.MultipleContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/multiple-content")
public class MultipleContentController {
    @Autowired
    MultipleContentService multipleContentService;

    @GetMapping(value = "/getAll")
    public ResponseEntity<ResponseBodyDTO<MultipleContent>> getAll(@RequestParam(required = false) String search, Pageable pageable) {
        Page<MultipleContent> page = multipleContentService.getAll(pageable, search);
        ResponseBodyDTO result = new ResponseBodyDTO(pageable, page);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<MultipleContentDTO> update(@RequestBody MultipleContentDTO request) throws Exception {
        MultipleContentDTO result = multipleContentService.update(request);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "/updateList")
    public ResponseEntity<Boolean> updateList(@RequestBody CreateListSmsRequest request) throws Exception {
        Boolean result = multipleContentService.updateList(request);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/getOne/{id}")
    public ResponseEntity<MultipleContentDTO> getOne(@PathVariable Integer id) {
        MultipleContentDTO result = multipleContentService.getOne(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/getOneUncompleted/{id}")
    public ResponseEntity<MultipleContentDTO> getOneUncompleted(@PathVariable Integer id) {
        MultipleContentDTO result = multipleContentService.getOneUncompleted(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<MultipleContent> delete(@PathVariable Integer id) {
        multipleContentService.delete(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
