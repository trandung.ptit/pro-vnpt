package com.trandung.autoimess.sms.api.request;

import com.trandung.autoimess.sms.model.MultipleContent;
import com.trandung.autoimess.sms.model.Receiver;
import lombok.Data;

import java.util.List;

@Data
public class MultipleContentDTO {
    public MultipleContent multipleContent;
    public List<Receiver> receivers;
    public Integer smsDurationFrom;
    public Integer smsDurationTo;
    public Integer templateId;
    public String templateName;
    public String status;
}
