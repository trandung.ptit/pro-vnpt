package com.trandung.autoimess.sms.api.request;

import com.trandung.autoimess.sms.model.Receiver;
import lombok.Data;

import java.util.List;
@Data
public class CreateListSmsRequest {
    public List<Receiver> receivers;
    public Integer smsDurationFrom;
    public Integer smsDurationTo;
    public Integer templateId;
    public Integer tabCount;
    public String notes;
}
