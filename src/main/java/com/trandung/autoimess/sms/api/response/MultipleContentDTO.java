package com.trandung.autoimess.sms.api.response;

import lombok.Data;
import java.time.Instant;

@Data
public class MultipleContentDTO {
    private Integer id;
    private Integer totalReceiver;
    private Integer totalReceived;
    private String note;
    private String time;
    private Instant createdDate;
    private Instant updateDate;
    private String status;
}
