package com.trandung.autoimess.sms.api.response;

import lombok.Data;

@Data
public class ReceiverDTO {
    private Integer id;
    private String note;
    private Integer multipleContentId;
    private String phoneNumber;
    private String smsContent;
    private Integer smsDuration;
    private Boolean isCompleted;
}
