package com.trandung.autoimess.sms.api;

import com.trandung.autoimess.sms.api.response.ReceiverDTO;
import com.trandung.autoimess.sms.service.ReceiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/receiver")
public class ReceiverController {
    @Autowired
    ReceiverService receiverService;


    @GetMapping(value = "/complete/{id}")
    public ResponseEntity<String> completeSend(@PathVariable Integer id) throws Exception {
        receiverService.completeSend(id);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @GetMapping(value = "/findAllByPhoneNumber/{phoneNumber}")
    public ResponseEntity<List<ReceiverDTO>> findAllByPhoneNumber(@PathVariable String phoneNumber) throws Exception {
        List<ReceiverDTO> receiverDTOS = receiverService.findAllByPhoneNumber(phoneNumber);
        return new ResponseEntity<>(receiverDTOS, HttpStatus.OK);
    }
}
