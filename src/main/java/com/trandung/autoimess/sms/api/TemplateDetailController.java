package com.trandung.autoimess.sms.api;

import com.trandung.autoimess.sms.dto.TemplateDetailDTO;
import com.trandung.autoimess.sms.model.TemplateDetail;
import com.trandung.autoimess.sms.service.TemplateDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/v1/template-detail")
public class TemplateDetailController {

    @Autowired
    TemplateDetailService templateDetailService;

    @GetMapping("/getAllByTemplateId/{id}")
    public ResponseEntity<List<TemplateDetailDTO>> getAllByTemplateId(@PathVariable Integer id) {
        return new ResponseEntity<>(templateDetailService.getAllByTemplateId(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<TemplateDetail> update(@RequestBody TemplateDetail templateDetail) {
        TemplateDetail result = templateDetailService.update(templateDetail);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/getOne/{id}")
    public ResponseEntity<TemplateDetailDTO> getOne(@PathVariable Integer id) {
        TemplateDetailDTO result = templateDetailService.getOne(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @DeleteMapping(value = "/{id}")
    public ResponseEntity<TemplateDetail> delete(@PathVariable Integer id) {
        templateDetailService.delete(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}