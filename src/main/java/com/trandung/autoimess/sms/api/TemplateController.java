package com.trandung.autoimess.sms.api;

import com.trandung.autoimess.sms.model.Template;
import com.trandung.autoimess.sms.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/template")
public class TemplateController {
    @Autowired
    TemplateService templateService;

    @GetMapping
    public ResponseEntity<List<Template>> getAll() {
        return new ResponseEntity<>(templateService.getAll(), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Template> update(@RequestBody Template template) {
        Template result = templateService.update(template);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Template> getOne(@PathVariable Integer id) {
        Template result = templateService.getOne(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Template> delete(@PathVariable Integer id) {
        templateService.delete(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
