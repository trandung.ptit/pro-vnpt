package com.trandung.autoimess.sms.service;

import com.trandung.autoimess.sms.api.request.CreateListSmsRequest;
import com.trandung.autoimess.sms.api.request.MultipleContentDTO;
import com.trandung.autoimess.sms.config.error.BadRequestException;
import com.trandung.autoimess.sms.mapper.GenericMapper;
import com.trandung.autoimess.sms.model.MultipleContent;
import com.trandung.autoimess.sms.model.Receiver;
import com.trandung.autoimess.sms.model.Template;
import com.trandung.autoimess.sms.model.TemplateDetail;
import com.trandung.autoimess.sms.repository.MultipleContentRepository;
import com.trandung.autoimess.sms.repository.ReceiverRepository;
import com.trandung.autoimess.sms.repository.TemplateDetailRepository;
import com.trandung.autoimess.sms.repository.TemplateRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Service
@Slf4j
public class MultipleContentService {
    @Autowired
    MultipleContentRepository multipleContentRepository;

    @Autowired
    ReceiverRepository receiverRepository;

    @Autowired
    TemplateDetailRepository templateDetailRepository;

    @Autowired
    TemplateRepository templateRepository;

    @Autowired
    GenericMapper genericMapper;


    public Page<MultipleContent> getAll(Pageable pageable, String search) {
        if (StringUtils.containsWhitespace(search)) {

        }
        Instant now = Instant.now();
        Page<MultipleContent> page = multipleContentRepository.findAll(pageable);
        page.getContent().stream().forEach(i -> {
            if (null != i.getUpdateDate() && i.getUpdateDate().plusSeconds(30L).isAfter(now)) i.setStatus("SENDING");
            if (i.getTotalReceived().intValue() >= i.getTotalReceiver().intValue()) i.setStatus("COMPLETED");
        });
        return page;
    }

    public MultipleContentDTO getOne(Integer id) {
        MultipleContentDTO multipleContentDTO = new MultipleContentDTO();
        multipleContentDTO.setMultipleContent(multipleContentRepository.findById(id).orElse(null));
        multipleContentDTO.setReceivers(receiverRepository.findAllByMultipleContentId(id));
        return multipleContentDTO;
    }

    public void delete(Integer id) {
        multipleContentRepository.deleteById(id);
    }

    @Async
    public MultipleContentDTO update(MultipleContentDTO multipleContentDTO) throws Exception {
        Random rand = new Random();
        int min = multipleContentDTO.getSmsDurationFrom();
        int max = multipleContentDTO.getSmsDurationTo();
        List<Receiver> receivers = multipleContentDTO.getReceivers();

        // Save multiple content
        MultipleContent multipleContent = multipleContentDTO.getMultipleContent() == null ? new MultipleContent() : multipleContentDTO.getMultipleContent();
        multipleContent.setTotalReceiver(receivers.size());
        multipleContent.setCreatedDate(Instant.now());
        multipleContent.setStatus("END");
        multipleContent.setTime(min + " - " + max);
        multipleContent.setTotalReceived(0);
        multipleContent = multipleContentRepository.save(multipleContent);

        // Get all template
        List<TemplateDetail> templateDetails = templateDetailRepository.findAllByTemplateIdOrderByIdDesc(multipleContentDTO.getTemplateId());
        if (templateDetails.isEmpty()) {
            throw new BadRequestException("Không có mẫu tin nhắn, vui lòng thêm mới");
        }
        // Random sms
        for (Receiver receiver : receivers) {
            receiver.setMultipleContentId(multipleContent.getId());
            String sms = templateDetails
                    .get(rand.nextInt(templateDetails.size()))
                    .getTemplateContent()
                    .replace("{content}", receiver.getSmsContent()); //Replace
            receiver.setSmsContent(sms);
            receiver.setIsCompleted(false);

            receiver.setSmsDuration(rand.nextInt(max - min + 1) + min);
        }

        receivers = receiverRepository.saveAll(receivers);

        //result
        multipleContentDTO.setMultipleContent(multipleContent);
        multipleContentDTO.setReceivers(receivers);

        return multipleContentDTO;
    }

    public MultipleContentDTO getOneUncompleted(Integer id) {
        Instant start = Instant.now();
        MultipleContentDTO multipleContentDTO = new MultipleContentDTO();
        multipleContentDTO.setMultipleContent(multipleContentRepository.findById(id).orElse(null));
        multipleContentDTO.setReceivers(receiverRepository.findAllByMultipleContentIdAndIsCompletedFalse(id));
        Instant end = Instant.now();
        Duration res = Duration.between(start, end);
        log.info("getOneUncompleted() with id {} take {}s", id, res.getSeconds());
        return multipleContentDTO;
    }

    public Boolean updateList(CreateListSmsRequest request) {
        int partitionSize = request.getReceivers().size() / request.getTabCount();
        if (request.getReceivers().size() % request.getTabCount() != 0) partitionSize++;
        List<List<Receiver>> partitions = ListUtils.partition(request.getReceivers(), partitionSize);
        for (List<Receiver> receivers : partitions) {
            Random rand = new Random();
            int min = request.getSmsDurationFrom();
            int max = request.getSmsDurationTo();

            // Save multiple content
            MultipleContent multipleContent = new MultipleContent();
            String note = request.getNotes() != null ? request.getNotes(): "Tự động chia";
            multipleContent.setNote(note + " " + (partitions.indexOf(receivers)+1));    // them space
            multipleContent.setTotalReceiver(receivers.size());
            multipleContent.setCreatedDate(Instant.now());
            multipleContent.setStatus("END");
            multipleContent.setTime(min + " - " + max);
            multipleContent.setTotalReceived(0);
            multipleContent = multipleContentRepository.save(multipleContent);

            // Get all template
            List<TemplateDetail> templateDetails = templateDetailRepository.findAllByTemplateIdOrderByIdDesc(request.getTemplateId());
            if (templateDetails.isEmpty()) {
                throw new BadRequestException("Không có mẫu tin nhắn, vui lòng thêm mới");
            }

            // Random sms
            for (Receiver receiver : receivers) {
                receiver.setMultipleContentId(multipleContent.getId());
                String sms = templateDetails
                        .get(rand.nextInt(templateDetails.size()))
                        .getTemplateContent()
                        .replace("{content}", receiver.getSmsContent()); //Replace
                receiver.setSmsContent(sms);
                receiver.setIsCompleted(false);

                receiver.setSmsDuration(rand.nextInt(max - min + 1) + min);
            }
            receiverRepository.saveAll(receivers);
        }
        return true;
    }
}
