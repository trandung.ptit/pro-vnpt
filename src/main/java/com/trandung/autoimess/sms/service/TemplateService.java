package com.trandung.autoimess.sms.service;

import com.trandung.autoimess.sms.config.error.BadRequestException;
import com.trandung.autoimess.sms.model.Template;
import com.trandung.autoimess.sms.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TemplateService {
    @Autowired
    TemplateRepository templateRepository;

    public Template update(Template template) {
        return templateRepository.save(template);
    }

    public Template getOne(Integer id) {
        Template template = templateRepository.findById(id).orElse(null);
        if (null == template) {
            throw new BadRequestException("Cú pháp không tồn tại ");
        }
        return template;
    }

    public void delete(Integer id) {
        templateRepository.deleteById(id);
    }

    public List<Template> getAll() {
        return templateRepository.findAll();
    }
}
