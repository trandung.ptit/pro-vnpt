package com.trandung.autoimess.sms.service;

import com.trandung.autoimess.sms.config.error.BadRequestException;
import com.trandung.autoimess.sms.dto.TemplateDetailDTO;
import com.trandung.autoimess.sms.mapper.GenericMapper;
import com.trandung.autoimess.sms.model.Template;
import com.trandung.autoimess.sms.model.TemplateDetail;
import com.trandung.autoimess.sms.repository.TemplateDetailRepository;
import com.trandung.autoimess.sms.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TemplateDetailService {
    @Autowired
    TemplateDetailRepository templateDetailRepository;

    @Autowired
    TemplateRepository templateRepository;

    @Autowired
    GenericMapper genericMapper;


    public List<TemplateDetailDTO> getAllByTemplateId(Integer templateId) {
        Template template = templateRepository.findById(templateId).orElse(null);
        if (null == template) {
            throw new BadRequestException("Cú pháp không tồn tại");
        }
        List<TemplateDetail> templateDetails = templateDetailRepository.findAllByTemplateIdOrderByIdDesc(templateId);
        List<TemplateDetailDTO> templateDetailDTOS = new ArrayList<>();

        for (TemplateDetail t : templateDetails) {
            TemplateDetailDTO templateDetailDTO = genericMapper.mapToType(t, TemplateDetailDTO.class);
            templateDetailDTO.setTemplateName(template.getName());
            templateDetailDTOS.add(templateDetailDTO);
        }
        return templateDetailDTOS;
    }

    public TemplateDetail update(TemplateDetail templateDetail) {
        return templateDetailRepository.save(templateDetail);
    }

    public TemplateDetailDTO getOne(Integer id) {
        TemplateDetail templateDetail = templateDetailRepository.findById(id).orElse(null);
        if (null == templateDetail) {
            throw new BadRequestException("Cú pháp không tồn tại ");
        }
        Template template = templateRepository.findById(templateDetail.getTemplateId()).orElse(new Template());
        TemplateDetailDTO templateDetailDTO = genericMapper.mapToType(templateDetail, TemplateDetailDTO.class);
        templateDetailDTO.setTemplateName(template.getName());
        return templateDetailDTO;
    }

    public void delete(Integer id) {
        templateDetailRepository.deleteById(id);
    }
}

