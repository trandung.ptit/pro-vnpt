package com.trandung.autoimess.sms.service;

import com.trandung.autoimess.sms.api.response.ReceiverDTO;
import com.trandung.autoimess.sms.config.error.BadRequestException;
import com.trandung.autoimess.sms.mapper.GenericMapper;
import com.trandung.autoimess.sms.model.MultipleContent;
import com.trandung.autoimess.sms.model.Receiver;
import com.trandung.autoimess.sms.repository.MultipleContentRepository;
import com.trandung.autoimess.sms.repository.ReceiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReceiverService {
    @Autowired
    ReceiverRepository receiverRepository;

    @Autowired
    MultipleContentRepository multipleContentRepository;

    @Autowired
    GenericMapper genericMapper;

    public void delete(Integer id) {
        receiverRepository.deleteById(id);
    }

    public void completeSend(Integer id) throws Exception {
        Receiver receiver = receiverRepository.findById(id).orElse(null);
        if (null == receiver) {
            throw new Exception("Id tin nhắn không tồn tại");
        }
        receiver.setIsCompleted(true);
        if (receiver.getMultipleContentId() != null) {
            MultipleContent multipleContent = multipleContentRepository.findById(receiver.getMultipleContentId()).orElse(null);
            if (multipleContent == null) {
                throw new BadRequestException("Id chiến dịch tin nhắn không tồn tại");
            }
            multipleContent.setTotalReceived(multipleContent.getTotalReceived() + 1);
            multipleContent.setUpdateDate(Instant.now());
            multipleContentRepository.save(multipleContent);
        }
        receiverRepository.save(receiver);
    }

    public List<ReceiverDTO> findAllByPhoneNumber(String phoneNumber) {
        List<ReceiverDTO> receiverDTOs= new ArrayList<>();
        List<Receiver> receivers = receiverRepository.findAllByPhoneNumber(phoneNumber);
        for (Receiver r : receivers) {
            ReceiverDTO receiverDTO = genericMapper.mapToType(r, ReceiverDTO.class);
            MultipleContent multipleContent = multipleContentRepository.findById(r.getMultipleContentId()).orElse(null);
            if(multipleContent!=null) {
                receiverDTO.setNote(multipleContent.getNote());
            }
            receiverDTOs.add(receiverDTO);
        }
        return receiverDTOs;
    }
}

