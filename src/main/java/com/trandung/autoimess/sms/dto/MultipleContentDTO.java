package com.trandung.autoimess.sms.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class MultipleContentDTO {
    private Integer id; // tự động gen
    private Integer totalReceiver;
    private Integer totalReceived;
    private String note;
    private String time;
    private Instant createdDate;
    private Instant updateDate;
    private String status;
    private String templateName;
}
