package com.trandung.autoimess.sms.dto;

import lombok.Data;

@Data
public class TemplateDetailDTO {
    private Integer id; // tự động gen
    private String templateId;
    private String templateName;
    private String templateContent;
    private String type;
}
