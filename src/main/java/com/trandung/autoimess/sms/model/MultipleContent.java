package com.trandung.autoimess.sms.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Data
@NoArgsConstructor
@Table(name = "multiple_content")
@Entity
public class MultipleContent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; // tự động gen

    @Column(name = "total_receiver")
    private Integer totalReceiver;

    @Column(name = "total_received")
    private Integer totalReceived;

    @Column(name = "note")
    private String note;

    @Column(name = "time")
    private String time;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "update_date")
    private Instant updateDate;

    @Column(name = "status")
    private String status;
}
