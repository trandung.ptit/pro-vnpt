package com.trandung.autoimess.sms.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Table(name = "receiver", indexes = {@Index(name = "multiple_content_id_index",  columnList="multiple_content_id")})
@Entity
public class Receiver {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; // tự động gen

    @Column(name = "multiple_content_id")
    private Integer multipleContentId;

    @Column
    private String phoneNumber; // sdt nguoi nhan

    @Column(columnDefinition = "text")
    private String smsContent; // noi dung moi tin nhan

    @Column
    private Integer smsDuration; // delay moi tin nhan

    @Column(name = "is_completed")
    private Boolean isCompleted;
}
