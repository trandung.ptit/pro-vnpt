package com.trandung.autoimess.sms.repository;

import com.trandung.autoimess.sms.model.Receiver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReceiverRepository extends JpaRepository<Receiver, Integer> {
    List<Receiver> findAllByMultipleContentId(Integer id);
    List<Receiver> findAllByMultipleContentIdAndIsCompletedFalse(Integer id);
    List<Receiver> findAllByPhoneNumber(String phoneNumber);
}
