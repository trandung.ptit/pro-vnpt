package com.trandung.autoimess.sms.repository;

import com.trandung.autoimess.sms.model.TemplateDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TemplateDetailRepository extends JpaRepository<TemplateDetail, Integer> {

    List<TemplateDetail> findAllByTemplateIdOrderByIdDesc(Integer templateId);

    List<TemplateDetail> findAllByOrderByIdDesc();
}
