package com.trandung.autoimess.sms.repository;

import com.trandung.autoimess.sms.model.MultipleContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MultipleContentRepository extends JpaRepository<MultipleContent, Integer> {
}
